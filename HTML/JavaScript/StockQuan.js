var stock;
var quantity;
var stockSell;
var quantitySell;

function Update(){

    stock=document.getElementById("Stock").value;
    quantity=document.getElementById("Quantity").value;

    if(parseFloat(quantity) <= 0){
        alert("Invalid action!");
    }
    else{
          alert("You have successfully trade " + quantity + " for " + stock);

        $.getJSON("https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed?ticker=" + stock, function(data) {

          var currentPrice = data['price_data'][0][1];
        
        
          var dataToSend = 
          {
            "dateCreated": "",
            "timeCreated": "",
            "stockTicker": stock,
            "stockQuantity": quantity,
            "requestedPrice": currentPrice*quantity,
            "tradeStatus": "CREATED",
            "tradeType": "BUY",
            "historic": true
          };
        
          $.ajax({
            type : "POST",
            contentType : "application/json;charset=utf-8",
            url : "http://localhost:8080/api/trades",
            data : JSON.stringify(dataToSend),
            dataType : 'json',              
            success : console.log("WORKED!")
          });
        });
      }
}

function Delete(){

  stockSell=document.getElementById("Stock").value;
  quantitySell=document.getElementById("Quantity").value;

  if(parseFloat(quantitySell) <= 0){
      alert("Invalid action!");
  }
  else{
        alert("You have successfully trade " + quantitySell + " for " + stockSell);

      $.getJSON("https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed?ticker=" + stockSell, function(data) {

        var currentPrice = data['price_data'][0][1];
      
      
        var dataToSend = 
        {
          "dateCreated": "",
          "timeCreated": "",
          "stockTicker": stockSell,
          "stockQuantity": quantitySell,
          "requestedPrice": currentPrice*quantitySell,
          "tradeStatus": "CREATED",
          "tradeType": "SELL",
          "historic": true
        };
      
        $.ajax({
          type : "POST",
          contentType : "application/json;charset=utf-8",
          url : "http://localhost:8080/api/trades",
          data : JSON.stringify(dataToSend),
          dataType : 'json',              
          success : console.log("WORKED!")
        });
      });
    }
}
